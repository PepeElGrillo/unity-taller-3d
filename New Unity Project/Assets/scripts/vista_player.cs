﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vista_player : MonoBehaviour
{
    public player_model model;
    public float Fsalto;


    // Start is called before the first frame update
    void Start()
    {
        model = GetComponent<player_model>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(model.velocidadm, GetComponent<Rigidbody>().velocity.y);
        }

        if (Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(-model.velocidadm, GetComponent<Rigidbody>().velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, Fsalto);
        }
    }
}
