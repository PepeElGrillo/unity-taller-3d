﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destruction_v : MonoBehaviour
{
    public destruction_m m;
    // Start is called before the first frame update
    void Start()
    {
        m = GetComponent<destruction_m>();
        m.thisparticleSystem = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m.thisparticleSystem.isPlaying)
            return;
        Destroy(gameObject);
    }
}
