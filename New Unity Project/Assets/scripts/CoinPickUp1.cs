﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickUp1 : MonoBehaviour {

    public int PointsToAdd;
    void OnTriggerEnter2D(Collider2D other) {
        if (other.GetComponent<PlayerController>() == null)
            return;
        ScoreManager.AddPoints(PointsToAdd);
        Destroy(gameObject);
    }
}