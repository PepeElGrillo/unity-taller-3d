﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript : MonoBehaviour
{
    public Rigidbody2D rb;
    public float velocidad_correr;
    public float fuerza_salto;
    public LayerMask groundLayerMask;
    public bool grounded;
    public Transform groundCheck;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 velocidad = new Vector2(0, rb.velocity.y);
        float horizontal = Input.GetAxis("Horizontal");

        velocidad.x = horizontal * velocidad_correr;

        rb.velocity = velocidad;

        if (Input.GetKeyDown(KeyCode.Space) /*&& (grounded == true)*/)
        {
            /*grounded = false;*/
            rb.AddForce(new Vector2(0, fuerza_salto));
        }
        /*DetectGround();*/
    }

    /*void DetectGround()
    {
        RaycastHit2D ray = Physics2D.BoxCast(
            new Vector2(transform.position.x + (GetComponent<BoxCollider2D>().offset.x * transform.localScale.x), transform.position.y),
            new Vector2(GetComponent<BoxCollider2D>().size.x, 0.1f),
            0,
            Vector2.down,
            Vector2.Distance(groundCheck.position, transform.position),
            groundLayerMask);
        if (ray)
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
    }*/
}
