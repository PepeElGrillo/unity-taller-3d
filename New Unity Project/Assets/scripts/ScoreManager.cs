﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour {
    public static int score;
    Text text;

	
	void Start () {
        text = GetComponent<Text>();
        score = 0;
	
	}
	
	
	void Update () {
        if (score < 0)
            score = 0;
        else if (score == 500) //si puntaje igual o mayor a 500 el pj gana 
        {
            SceneManager.LoadScene("Win", LoadSceneMode.Single); 
            Debug.Log("Victoria");
        }
        text.text = "" + score;
	
	}
    public static void AddPoints (int PointsToAdd)
    {
        score += PointsToAdd;
    }
    public static void Reset()
    {
        score = 0;
    }
}
