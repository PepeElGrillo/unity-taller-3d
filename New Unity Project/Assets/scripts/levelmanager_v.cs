﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelmanager_v : MonoBehaviour
{
    public levelmanager_m m;

    // Start is called before the first frame update
    void Start()
    {
        m = GetComponent<levelmanager_m>();
        m.player = FindObjectOfType<vista_player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RespawndPlayer()
    {
        StartCoroutine("RespawnPlayerCo");
    }
    public IEnumerator RespawnPlayerCo()
    {
        Instantiate(m.deathParticle, m.player.transform.position, m.player.transform.rotation);
        m.player.enabled = false;
        m.player.GetComponent<Renderer>().enabled = false;
        ScoreManager.AddPoints(-m.pointpenaltyOnDeath);
        Debug.Log("Reaparece el Player");
        yield return new WaitForSeconds(m.respawDelay);
        m.player.transform.position = m.currentCheckpoint.transform.position;
        m.player.enabled = true;
        m.player.GetComponent<Renderer>().enabled = true;
        Instantiate(m.respawParticle, m.currentCheckpoint.transform.position, m.currentCheckpoint.transform.rotation);

    }
}
