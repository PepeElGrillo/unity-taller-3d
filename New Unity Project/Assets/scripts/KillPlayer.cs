﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class KillPlayer : MonoBehaviour {
    public LevelManager levelManager;
	// Use this for initialization
	void Start () {
        levelManager = FindObjectOfType<LevelManager>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "player" && ScoreManager.score > 0) 
        {

            levelManager.RespawndPlayer();
        }
        else //si puntaje igual 0 el pj muere pa siempre
        {
            TotalDefeat();
        }
    }
    public void TotalDefeat()
    {
        SceneManager.LoadScene("Defeat", LoadSceneMode.Single); 
        Debug.Log("Derrota");
    }
}
